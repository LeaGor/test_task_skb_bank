<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%clients}}`.
 */
class m190209_130341_create_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%clients}}', [
            'id'            => $this->primaryKey(),
            'surname'       => $this->string(255)->notNull(),
            'name'          => $this->string(255)->notNull(),
            'patronymic'    => $this->string(255)->notNull(),
            'birth_date'    => $this->date()->notNull(),
            'phone'         => $this->decimal(11,0)->notNull(),
            'user_ip'       => $this->string(255)->notNull(),
            'user_agent'    => $this->text()->notNull(),
            'updated_at'    => $this->timestamp(),
            'created_at'    => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%clients}}');
    }
}
