<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Client;

class SiteController extends Controller
{
    public function actionIndex()
    {
        $model = new Client();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->birth_date = \DateTime::createFromFormat('d.m.Y', $model->birth_date)->format('Y-m-d');
            $model->phone = str_replace(['(',')',' ','-'],'',$model->phone);
            $model->save();
            Yii::$app->session->set('thanks', true);
            return $this->redirect(['thanks']);
        }
        return $this->render('main', ['model' => $model]);
    }

    public function actionThanks()
    {
        if(!Yii::$app->session->get('thanks')){
            return $this->redirect(['/']);
        }

        return $this->render('thanks');
    }
}
