<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Client extends ActiveRecord
{
    function __construct(array $config = [])
    {
        parent::__construct($config);
        if(!isset($this->user_ip)){
            $this->user_ip = Yii::$app->request->userIP;
        }
        if(!isset($this->user_agent)){
            $this->user_agent = Yii::$app->request->userAgent;
        }
    }

    public static function tableName()
    {
        return 'clients';
    }

    public function rules()
    {
        return [
            [['surname', 'name', 'patronymic', 'birth_date', 'phone'], 'required', 'message' => '*'],
            [['surname', 'name', 'patronymic'], 'match', 'pattern' => '/^[а-яёА-ЯЁ]{2,}$/iu'],
            //[['birth_date'], 'date', 'format' => 'dd.MM.yyyy']
            //[['birth_date'], 'date', 'format' => 'php:d.m.Y']
        ];
    }
}