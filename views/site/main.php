<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 main__top">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-md-push-6 main__top-list">
                        <h1>Отправьте онлайн заявку на кредит!</h1>
                        <h2>Понятные условия</h2>
                        <ul>
                            <li>На любые цели до 1 300 000 рублей</li>
                            <li>Единая ставка от 19,9% годовых на весь срок до 5 лет</li>
                            <li>Досрочное погашение без ограничений</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-md-10 col-md-push-1 col-lg-8 col-lg-push-2 main__form">
                        <div class="row">
                            <?php $form = ActiveForm::begin([
                                'options' => [
                                    'class' => 'col-xs-12 col-sm-10 col-sm-push-1 main__form-form'
                                ]
                            ]); ?>
                                <div class="main__form-lenta">
                                    <img src="/assets/test/lenta.png">
                                    <p><span>топ-10</span> самых дешевых кредитов*</p>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xs-12 col-sm-3 main__form-city__label">Город</label>
                                    <a href="#" class="col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 main__form-city__city">Екатеринбург</a>
                                </div>
                                <div class="form-group row">
                                    <?=
                                    $form
                                        ->field($model, 'surname')
                                        ->textInput(['class' => 'col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 main__form-input'])
                                        ->label('Фамилия',['class'=>'col-xs-12 col-sm-3'])
                                    ?>
                                </div>
                                <div class="form-group row">
                                    <?=
                                    $form
                                        ->field($model, 'name')
                                        ->textInput(['class' => 'col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 main__form-input'])
                                        ->label('Имя',['class'=>'col-xs-12 col-sm-3'])
                                    ?>
                                </div>
                                <div class="form-group row">
                                    <?=
                                    $form
                                        ->field($model, 'patronymic')
                                        ->textInput(['class' => 'col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 main__form-input'])
                                        ->label('Отчество',['class'=>'col-xs-12 col-sm-3'])
                                    ?>
                                </div>
                                <div class="form-group row">
                                    <?=
                                    $form
                                        ->field($model, 'birth_date')
                                        ->textInput(['class' => 'col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 main__form-input'])
                                        ->label('Дата рождения',['class'=>'col-xs-12 col-sm-3'])
                                    ?>
                                </div>
                                <div class="form-group row">
                                    <?=
                                    $form
                                        ->field($model, 'phone')
                                        ->textInput(['class' => 'col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 main__form-input'])
                                        ->label('Телефон',['class'=>'col-xs-12 col-sm-3'])
                                    ?>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xs-12 col-sm-3">E-mail</label>
                                    <input class="col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 main__form-input">
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3"></div>
                                    <div class="col-xs-12 col-sm-9 main__form-consent">
                                        <input class="hidden" data-item="main__form-consent-hidden_checkbox1" type="checkbox" checked>
                                        <input class="hidden" data-item="main__form-consent-hidden_checkbox2" type="checkbox">
                                        <p data-block="main__form-consent-hidden_checkbox1">
                                            <span class="main__form-consent-checkbox main__form-consent-checkbox_checked"></span>
                                            Я не против получать выгодные предложения на этот адрес
                                        </p>
                                        <p data-block="main__form-consent-hidden_checkbox2">
                                            <span class="main__form-consent-checkbox"></span>
                                            Я принимаю условия
                                            <a href="#" class="main__form-consent-link">банковского соглашения</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <?= Html::submitButton('получить кредит', ['class' => 'col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-3 main__form-submit']) ?>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-10 col-xs-push-1">
                        <div class="row">
                            <div class="col-md-8 col-md-push-2 main__process">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="main__process-title">Простое оформление</p>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-lg-6 main__process-block">
                                                <span class="main__process-step_1-numb">1</span>
                                                <p class="main__process-step_1-text">Отправьте заявку — мы вам перезвоним!<br>Узнайте у специалиста банка предварительное решение</p>
                                            </div>
                                            <div class="col-xs-12 col-lg-1 main__process-block main__process-step_chevron">
                                                <img src="/assets/test/chevron.png">
                                            </div>
                                            <div class="col-xs-12 col-lg-5 main__process-block">
                                                <span class="main__process-step_2-numb">2</span>
                                                <p class="main__process-step_2-text">Приходите в офис для оформления документов</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>