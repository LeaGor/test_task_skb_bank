<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-10 col-xs-push-1 col-md-10 col-md-push-1 col-lg-8 col-lg-push-2">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 main__thanks">
                                <h1>Спасибо за обращение!<br>Ваша заявка принята в работу</h1>
                                <p>Для оформления кредита Вы можете подать документы в любом офисе СКБ-банка</p>
                                <ul>
                                    <li><p>На сумму до <b>180 000 рублей</b> Вам потребуется паспорт и второй документ (на выбор - страховое свидетельство пенсионного фонда/СНИЛС, водительское удостоверение, загран. паспорт, удостоверение военнослужащего).</p></li>
                                    <li><p>Для получения <b>пониженной ставки или суммы до 1 300 000 рублей</b> - паспорт, второй документ и справка о доходах за последние 6 месяцев.</li>
                                    <li><p>Для <b>мужчин младше 27 лет</b> - обязательно наличие военного билета.</p></li>
                                </ul>
                                <a href="#" class="col-xs-10 col-xs-push-1 col-sm-4 col-sm-push-4 main__thanks_link">Выбрать офис</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
