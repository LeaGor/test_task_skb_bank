<?php
use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/test/bootstrap.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="Stylesheet">
    <link href="/test/style.css" rel="stylesheet">
    <link href="/test/media.css" rel="stylesheet">
</head>
<body>
<?php $this->beginBody() ?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 header__logo">
                <a href="/">
                    <img src="/test/logo.png" alt="СКБ банк">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 header__info">
                <p class="header__info-info">
                    Бесплатный звонок по россии
                </p>
                <p class="header__info-phone">
                    8 800 1000 600
                </p>
            </div>
        </div>
    </div>
</header>
<?= $content ?>
<footer>
    <div class="container">
        <div class="row footer">
            <div class="col-xs-12 col-md-4 col-md-push-8">
                <p class="footer__phone">8-800-1000-600</p>
                <p>Бесплатный звонок по России</p>
            </div>
            <div class="col-xs-12 col-md-4">
                <a href="#">Подробнее об условиях и видах кредитов</a>
                <p class="visible-md visible-lg">&nbsp;</p>
                <p>* По данным рейтинга портала Сравни.ру — <a href="#">подробнее</a></p>
            </div>
            <div class="col-xs-12 col-md-4 col-md-pull-8">
                <p>© СКБ-банк, 2012</p>
                <p>Генеральная лицензия № 705</p>
                <p>Центрального банка Российской Федерации</p>
            </div>
        </div>
    </div>
</footer>
<script src="/test/jquery.js"></script>
<script src="/test/bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ru.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<?php /*$this->endBody() */?>
<script>
    $(function() {
        $("#client-birth_date").datepicker({
            format: 'dd.mm.yyyy',
            startView: '2',
            language: 'ru',
        })

        //$.mask.definitions['&']='[А-Яа-я]';
        $("#client-phone").mask("+7(999) 999-9999");

        let block_name='main__form-consent'
        $('.'+block_name+' p').on('click', function () {
            let item = $(this).attr('data-block')
            $('[data-item='+item+']').click()
            if($(this).children('.'+block_name+'-checkbox').hasClass(block_name+'-checkbox_checked')){
                $(this).children('.'+block_name+'-checkbox').removeClass(block_name+'-checkbox_checked')
            } else {
                $(this).children('.'+block_name+'-checkbox').addClass(block_name+'-checkbox_checked')
            }
        })
    });
</script>
</body>
</html>
<?php $this->endPage() ?>
